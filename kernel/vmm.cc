#include "vmm.h"
#include "machine.h"
#include "idt.h"
#include "libk.h"
#include "locks.h"

struct PhysMemNode {
    PhysMemNode* next;
};

struct PhysMemInfo {
    PhysMemNode *firstFree;
    uint32_t avail;
    uint32_t limit;
    BlockingLock lock;
};

static PhysMemInfo *info = nullptr;

void PhysMem::init(uint32_t start, uint32_t end) {
    info = new PhysMemInfo;
    info->avail = start;
    info->limit = end;
    info->firstFree = nullptr;

    /* register the page fault handler */
    IDT::trap(14,(uint32_t)pageFaultHandler,3);
}

uint32_t PhysMem::alloc() {
    uint32_t p;

    info->lock.lock();

    if (info->firstFree != nullptr) {
        p = (uint32_t) info->firstFree;
        info->firstFree = info->firstFree->next;
    } else {
        if (info->avail == info->limit) {
            Debug::panic("no more frames");
        }
        p = info->avail;
        info->avail += FRAME_SIZE;
    }
    //Debug::printf("alloc %x\n",p);
    info->lock.unlock();

    bzero((void*)p,FRAME_SIZE);


    return p;
}

void PhysMem::free(uint32_t p) {
    info->lock.lock();

    //Debug::printf("free %x\n",p);
    PhysMemNode* n = (PhysMemNode*) p;    
    n->next = info->firstFree;
    info->firstFree = n;

    info->lock.unlock();
}

/****************/
/* AddressSpace */
/****************/

void AddressSpace::activate() {
    //Debug::printf("activate %x\n",(uint32_t)pd);
    vmm_on((uint32_t)pd);
}

AddressSpace::AddressSpace() : lock(new BlockingLock()) {
    pd = (uint32_t*) PhysMem::alloc();
    for (uint32_t va = PhysMem::FRAME_SIZE;
        va < info->limit;
        va += PhysMem::FRAME_SIZE
    ) {
        pmap(va,va,false,true);
    }
}

AddressSpace::~AddressSpace() {
	for (int i0 = 0; i0 < 1024; i0++) {
		uint32_t pde = pd[i0];
        if (pde & P) {
            uint32_t *pt = (uint32_t*) (pde & 0xfffff000);
            if (i0 > 0) {
                for (uint32_t i1 = 0; i1 < 1024; i1++) {
                    uint32_t pte = pt[i1];
                    if (pte & P) {
                        uint32_t pa = pte & 0xfffff000;
                        PhysMem::free(pa);
                    }
                }
            }
            PhysMem::free((uint32_t) pt);
        }
    }
    PhysMem::free((uint32_t) pd);
}

/* precondition: table is locked */
uint32_t& AddressSpace::getPTE(uint32_t va) {
    uint32_t i0 = (va >> 22) & 0x3ff;
    if ((pd[i0] & P) == 0) {
        pd[i0] = PhysMem::alloc() | 7; /* UWP */
    }
    uint32_t* pt = (uint32_t*) (pd[i0] & 0xfffff000);
    return pt[(va >> 12) & 0x3ff];
}

void AddressSpace::pmap(uint32_t va, uint32_t pa, bool forUser, bool forWrite) {
    //Debug::printf("map va:%x pa:%x\n",va,pa);
    invlpg(va);
    getPTE(va) = (pa & 0xfffff000) |
          (forUser ? U : 0) | (forWrite ? W : 0) | 1;
}

void AddressSpace::fork(StrongPtr<AddressSpace> child) {
    for (int i0 = 512; i0 < 1024; i0++) {
        uint32_t pde = pd[i0];
        if (pde & P) {
            uint32_t *pt = (uint32_t*) (pde & 0xfffff000);
            uint32_t high = i0 << 22;
            for (uint32_t i1 = 0; i1 < 1024; i1++) {
                uint32_t pte = pt[i1];
                if (pte & P) {
                    uint32_t va = high | (i1 << 12);
                    uint32_t src = pte & ~0xfff;
                    uint32_t dest = PhysMem::alloc();
                    memcpy((void*)dest,(void*)src,PhysMem::FRAME_SIZE);
                    child->pmap(va,dest,true,true);
                }
            }
        }
    }
}

void AddressSpace::handlePageFault(uint32_t va_) {
    if (va_ < 0x80000000) {
		Thread::exit();
    }

    uint32_t va = (va_ / PhysMem::FRAME_SIZE) * PhysMem::FRAME_SIZE;

    lock->lock();

    if ((getPTE(va) & 1)  == 0) {
        uint32_t pa = PhysMem::alloc();
        pmap(va,pa,true,true);
    }

    lock->unlock();
}

extern "C" void vmm_pageFault(uintptr_t va) {
    StrongPtr<AddressSpace> as = Thread::current()->process->addressSpace;
    Thread::current()->process->addressSpace->handlePageFault(va);
}
