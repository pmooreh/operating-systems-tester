#include "process.h"
#include "machine.h"
#include "locks.h"

struct ProcessInfo {
    StrongPtr<Process> kernelProcess;
    volatile int nextId;
};

static ProcessInfo *info;

void Process::init(void) {
    info = new ProcessInfo();
    info->nextId = 0;
    info->kernelProcess = StrongPtr<Process> { new Process() };
}

StrongPtr<Process> Process::kernel(void) {
    return info->kernelProcess;
}


Process::Process() :
	addressSpace(new AddressSpace()),
	id(getThenIncrement(&info->nextId,1)),
	fileLock(new BlockingLock()),
	semaLock(new BlockingLock()) {}

Process::~Process() {
    for (int i=0; i<NFILES; i++) {
        files[i].reset();
    }

	for (int i = 0; i < NSEMAPHORES; i++) {
		semaphores[i].reset();
	}

	// Debug::printf("DELETE");	

}

int Process::addFile(StrongPtr<File> file) {
    fileLock->lock();
    int lowest = 0;

    while (lowest < 100 && !files[lowest].isNull())
        lowest++;

    if (lowest == 100) {
        fileLock->unlock();
        return -1;
    }

    StrongPtr<OpenFile> openFile = StrongPtr<OpenFile>(new OpenFile(file));
    files[lowest] = openFile;
    fileLock->unlock();
    return lowest;
}

uint32_t Process::getFileLength(int fileDescriptor) {
	if (fileDescriptor < 0 || fileDescriptor >= NFILES)
		return -1;

	fileLock->lock();

    if (files[fileDescriptor].isNull()) {
        fileLock->unlock();
        return -1;
    }

    uint32_t res = files[fileDescriptor]->file->getLength();

    fileLock->unlock();
    return res;
}

ssize_t Process::readFile(int fileDescriptor, void* buf, size_t bytes) {
    if (fileDescriptor < 0 || fileDescriptor >= NFILES)
		return -1; 

	if ((uint32_t)buf < 0x80000000)
		return -1;

	if ((int)bytes < 0)
		return -1;

	uint32_t maxBytes = 0 - (uint32_t)buf;
	//Debug::printf("bytes able to write = %d\n", maxBytes);

	if (maxBytes < bytes)
		bytes = maxBytes;

	fileLock->lock();

    if (files[fileDescriptor].isNull()) {
        fileLock->unlock();
        return -1;
    }

    ssize_t res = files[fileDescriptor]->read(buf, bytes);

    fileLock->unlock();
    return res;
}

void Process::closeFile(int fileDescriptor) {
	if (fileDescriptor < 0 || fileDescriptor >= NFILES)
		return;

	fileLock->lock();

    if (files[fileDescriptor].isNull()) {
        fileLock->unlock();
        return;
    }

    files[fileDescriptor].reset();

    fileLock->unlock();
    return;
}

size_t Process::seekFile(int fileDescriptor, size_t offset) {
    if (fileDescriptor < 0 || fileDescriptor >= NFILES)
		return -1;

	if ((int)offset < 0)
		return -1;

	fileLock->lock();

	if (files[fileDescriptor].isNull()) {
		fileLock->unlock();
		return -1;
	}

	size_t res = files[fileDescriptor]->seek(offset);
	fileLock->unlock();

	return res;
}

void Process::setSharedInfo(StrongPtr<Process> parent) {
    // files
	for (int i = 0; i < NFILES; i++) {
        files[i] = parent->files[i];
    }

	// semaphores
	for (int i = 0; i < NSEMAPHORES; i++) {
		semaphores[i] = parent->semaphores[i];
	}

	return;
}

// semaphore methods //
int Process::addSemaphore(int count) {
	if (count < 0)
		return -1;

	semaLock->lock();

	// look through semaphores, find lowest
	int lowest = 0;
	while (lowest < NSEMAPHORES && !semaphores[lowest].isNull()) {
		lowest++;
	}

	if (lowest == NSEMAPHORES) {
		semaLock->unlock();
		return -1;
	}

	semaphores[lowest] = StrongPtr<Semaphore>(new Semaphore(count));
	semaLock->unlock();
	return lowest;
}

int Process::semaUp(int semId) {
	if (semId < 0 || semId >= NSEMAPHORES)
		return -1;
	if (semaphores[semId].isNull()) {
		return -1;
	}

	semaphores[semId]->up();
	return 0;
}
	
int Process::semaDown(int semId) {
	if (semId < 0 || semId >= NSEMAPHORES)
		return -1;
	if (semaphores[semId].isNull())
		return -1;
	
	semaphores[semId]->down();
	return 0;
}

StrongPtr<Semaphore> Process::getSemaphore(int id) {
	return semaphores[id];
}

//////////////
// OpenFile //
//////////////

OpenFile::OpenFile(StrongPtr<File> file) : offset(0), file(file), lock(new BlockingLock()) {
}

ssize_t OpenFile::read(void* buf, size_t nbyte) {
    lock->lock();

    ssize_t n = file->read(offset,buf,nbyte);
    if (n > 0) {
        offset += n;
    }

    lock->unlock();

    return n;
}

ssize_t OpenFile::write(void* buf, size_t nbyte) {
    lock->lock();

    ssize_t n = file->write(offset,buf,nbyte);
    if (n > 0) {
        offset += n;
    }

    lock->unlock();

    return n;
}

size_t OpenFile::seek(size_t newOffset) {
    lock->lock();

    size_t oldOffset = offset;
    offset = newOffset;

    lock->unlock();
    return oldOffset;
}

