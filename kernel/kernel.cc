#include "refs.h"
#include "device.h"
#include "fat439.h"
#include "ide.h"
#include "elf.h"
#include "kernel.h"

KernelInfo *kernelInfo = nullptr;

void kernelMain(void) {
    StrongPtr<BlockIO> d = IDE::d();
    StrongPtr<FileSystem> fs { new Fat439(d) };
    kernelInfo = new KernelInfo();
    kernelInfo->rootDir = fs->rootdir;
    StrongPtr<File> init = kernelInfo->rootDir->lookupFile("init");
    uint32_t e = ELF::load(init);
    switchToUser(e,0xfffff000,0x12345678);
}

void kernelTerminate(void) {
}
