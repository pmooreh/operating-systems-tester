#include "sys.h"
#include "stdint.h"
#include "idt.h"
#include "debug.h"
#include "thread.h"
#include "locks.h"
#include "kernel.h"
#include "elf.h"
#include "pipe.h"

BlockingLock* SYS::lock;

struct SwitchParam {
    uint32_t pc;
    uint32_t esp;
	uint32_t eax;
    SwitchParam(uint32_t pc, uint32_t esp, uint32_t eax)
        : pc(pc), esp(esp), eax(eax) {}
    ~SwitchParam() {}
};

void forkSwitch(SwitchParam param) {
    switchToUser(param.pc, param.esp, param.eax);
}

bool validEsp(uint32_t esp, uint32_t nargs) {
	bool valid = true;
	
	valid = valid && esp >= 0x80000000;

	uint32_t upperBound = esp + 4 * (nargs + 1) - 1;

	valid = valid && upperBound > esp;

	return valid;
}

int divideAndCeil(int dividend, int divisor) {
    int ret = dividend / divisor;
    if (dividend % divisor != 0)
        ret++;
    return ret;
}

bool validAddr(uint32_t addr) {
	return addr >= 0x80000000 && addr <= 0xffffffff;
}

int getStringLength(char* str) {
    int len = 0;
	uint32_t curr = (uint32_t)str;

    while (true) {
        if (!validAddr(curr))
			return -1;
		if (str[len] != 0) {
			len++;
			curr++;
		}
		else
			break;
	}

    return len + 1;
}

bool validPipeDescriptor(int a, int b) {
	uint32_t in = (uint32_t)a;
	uint32_t out = (uint32_t)b;

	if (in < 0x80000000 || out < 0x80000000)
		return false;

	if (in + 3 < in || out + 3 < out)
		return false;
	
	if (out < in + 4 && out >= in)
		return false;

	if (in < out + 4 && in >= out)
		return false;

	return true;
}

/*
    my research shows that magic[3] has the stack frame
    we want, and stack[1] has the variable we want! yay

number   signature                        description
0        void exit();                     terminate the current thread
1        write
2        int id= fork()                   returns twice
                                              id == 0 --> child
                                              id != 0 --> parent 
3        int id = semaphore(int count)    creates a semaphore with the
                                          given count
4        void up(int id)                  up
5        void down(int id)                down
6        void shutdown()                  shutdown
7        void join(int id)                block until the given process

8        open file
9        close file
10       read
11       length
12       seek
13       exec

*/
extern "C" int sysHandler(uint32_t eax, uint32_t *magic) {
    uint32_t* userStack = (uint32_t*)magic[3];

	//Debug::printf("stack: %x\n", (uint32_t)userStack);	
//	if ((uint32_t)userStack < 0x80000000)
//		return -1;

//	uint32_t arg = userStack[1];
//  uint32_t arg1 = userStack[2];
//  uint32_t arg2 = userStack[3];
    switch (eax) {
        case 0: {
			if (!validEsp((uint32_t)userStack, 1)) {
				return -1;
			}
			uint32_t arg = userStack[1];

			// exit(int status)
			// to report the status, store it in an array
			// which can be read after this thread has died
			StrongPtr<ThreadRemains> remains = Thread::remains()[Thread::current()->id];
			if (!remains.isNull())
				remains->exitStatus = (int)arg;
			remains.reset();
			// Debug::printf("exit status for thread %d: %d\n", Thread::current()->id, arg);
			Thread::exit();
			return 0;
			break;
		}
        case 1: {
            // write
            if (!validEsp((uint32_t)userStack, 3)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];
			uint32_t arg1 = userStack[2];
			uint32_t arg2 = userStack[3];

			if (!validAddr(arg1))
				return -1;

            int fd = (int) arg;
            void* buf = (void*) arg1;
            size_t nbyte = (size_t) arg2;
            if (fd < 0) return -1;
            if (fd >= Process::NFILES) return -1;
            StrongPtr<Process> me = Thread::current()->process;
            StrongPtr<OpenFile> of = me->files[fd];
            if (of.isNull()) return -1;
            return of->write(buf,nbyte);
			break;					/* XXX security */
        }
        case 2: {
            if (!validEsp((uint32_t)userStack, 0)) {
                return -1; 
            }   

			SYS::lock->lock();

			SwitchParam param = SwitchParam(magic[0], magic[3], 0);
            // create new process
            StrongPtr<Process> child = StrongPtr<Process>(new Process());
            // copy the address space
			Thread::current()->process->addressSpace->fork(child->addressSpace);
			// set files and sech same for parents and kiddos
            child->setSharedInfo(Thread::current()->process);
            // create new thread
            Thread* cThread = new FuncThread<SwitchParam>(child, forkSwitch, param);
            // now add child's thread's exitEvent; will serve as the id of the child
			int cId = cThread->id;
			int pId = Thread::current()->id;

			// create new ThreadRemains object for the newly created thread
			StrongPtr<ThreadRemains> rem = StrongPtr<ThreadRemains>(
					new ThreadRemains(cThread->exitEvent, pId));
			// set this child threads elemtnt accordingly in master list
			Thread::remains()[cId] = rem;

			// add the curent thread to the exit event array too? NO!

			// make this thread run!!!
            Thread::start(cThread);
			
		   	//Debug::printf("this id: %d\n", Thread::current()->id);
			//Debug::printf("child id: %d\n", cId);
			
			SYS::lock->unlock();

			return cId;
			break;
        }
        case 3: {
            if (!validEsp((uint32_t)userStack, 1)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];

			// get semaphore with count arg
			int id = Thread::current()->process->addSemaphore(arg);
            return id;
			break;
        }
        case 4: {
            if (!validEsp((uint32_t)userStack, 1)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];

			// Thread::current()->process->getSemaphore(arg)->up();
			return Thread::current()->process->semaUp(arg);
			break;
        }
        case 5: {
            if (!validEsp((uint32_t)userStack, 1)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];

			// Thread::current()->process->getSemaphore(arg)->down();
			return Thread::current()->process->semaDown(arg);
        }
        case 6: {
            Debug::panic("shut 'er down");
            return 0;
			break;
        }
        case 7: {
            if (!validEsp((uint32_t)userStack, 1)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];

            //Debug::printf("    join w/ id: %d\n", arg);
            
			// if the thread to join doesn't exist or has been consumed, ret -1
			if (Thread::remains()[arg].isNull())
				return -1;
			StrongPtr<ThreadRemains> remains = Thread::remains()[arg];
			
			// make sure the thread to be joined is the child
			int pid = Thread::current()->id;
			if (remains->parentId != pid)
				return -1;
			
			remains->childExitEvent->wait();

			int es = remains->exitStatus;

			Thread::remains()[arg].reset();

			return es;
			break;
        }
        case 8: {
            if (!validEsp((uint32_t)userStack, 1)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];

            // open file!
		
			if ((uint32_t)arg < 0x7fffffff)
				return -1;

			if (getStringLength((char*)arg) == -1) {
				return -1;
			}

			StrongPtr<File> file = kernelInfo->rootDir->lookupFile((char*)arg);

			// delete[] fileName;

            if (file.isNull()) {
                return -1;
			}

            int fileDescriptor = Thread::current()->process->addFile(file);

            return fileDescriptor;
			break;
        }
        case 9: {
            if (!validEsp((uint32_t)userStack, 1)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];

            // close file!
            Thread::current()->process->closeFile(arg);
            return 0;
			break;
        }
        case 10: {
            if (!validEsp((uint32_t)userStack, 3)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];
            uint32_t arg1 = userStack[2];
            uint32_t arg2 = userStack[3];

            // read! 3 args this time
            // ssize_t read(int fd, void* buf, size_t nbyte)
            
            //Debug::printf("arg0: %d, arg1: %x, arg2: %d\n", arg, arg1, arg2);
            
            return Thread::current()->process->readFile(arg, (void*)arg1, (size_t)arg2);
			break;
		}
        case 11: {
            // length
            if (!validEsp((uint32_t)userStack, 1)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];

            return Thread::current()->process->getFileLength(arg);
			break;
		}
        case 12: {
            // size_t seek(int fd, size_t offset)
            if (!validEsp((uint32_t)userStack, 2)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];
            uint32_t arg1 = userStack[2];

            return Thread::current()->process->seekFile(arg, arg1);
			break;
        }
        case 13: {
            //exec!
            if (!validEsp((uint32_t)userStack, 2)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];
            uint32_t arg1 = userStack[2];

			if (!validAddr((uint32_t)arg))
				return -1;

			if (getStringLength((char*)arg) == -1)
				return -1;

            StrongPtr<File> file = kernelInfo->rootDir->lookupFile((char*)arg);

            if (file.isNull()) {
                return -1;
            }

            int numArgs = arg1;

			if (!validEsp((uint32_t)userStack, 2 + arg1)) {
				return -1;
			}

            char** argArr = new char*[numArgs];
            int* argLengthArr = new int[numArgs];

            for (int i = 0; i < numArgs; i++) {
				int argLen = getStringLength((char*)(userStack[i + 3]));
				if (argLen < 0)
					return -1;
				argLengthArr[i] = argLen;
                argArr[i] = new char[argLen];
                memcpy((void*)argArr[i], (void*)userStack[i + 3], argLen);
                // Debug::printf("arg number %d: %s\n", i, argArr[i]);
            }

            // Debug::printf("stack is where? %x\n", magic[3]);
            // Debug::printf("stack according to thread? %x\n", Thread::current()->esp);

            // at this point: get rid of the address space,
            // then execute the new file

            StrongPtr<AddressSpace> newSpace = StrongPtr<AddressSpace>(new AddressSpace());
            newSpace->activate();
			Thread::current()->process->addressSpace = newSpace;
            newSpace.reset();
   
            uint32_t e = ELF::load(file);

            // Debug::printf("stack is where? %x\n", magic[3]);
            // Debug::printf("stack according to thread? %x\n", Thread::current()->esp);

            // add everything to the stack at 0xfffff000

            uint32_t newEsp = 0xfffff000;

            uint32_t* argAddrs = new uint32_t[numArgs];

            // put char literals on string
            for (int i = 0; i < numArgs; i++) {
                int subtract = 4 * divideAndCeil(argLengthArr[numArgs - 1 - i], 4);

                newEsp = newEsp - subtract;
                argAddrs[i] = newEsp;
                memcpy((void*)newEsp, (void*)argArr[numArgs - 1 - i], argLengthArr[numArgs - 1 - i]);
            }

            // for (int i = 0; i < 30; i++) {
            //     Debug::printf("%x: %c\n", newEsp + i, ((char*)newEsp)[i]);
            // }

            // put the strings on the stack
            for (int i = 0; i < numArgs; i++) {
                newEsp -= 4;
                ((uint32_t*)newEsp)[0] = argAddrs[i];
            }

            // put pointer to pointers
            newEsp -= 4;
            ((uint32_t*)newEsp)[0] = newEsp + 4;

            // finally put num args
            newEsp -= 4;
            ((uint32_t*)newEsp)[0] = (uint32_t)numArgs;


            // for (int i = 0; i < numArgs + 2; i++) {
            //     Debug::printf("index %d, address %x: %x\n", i, newEsp + 4*i, ((uint32_t*)newEsp)[i]);
            // }

            // get rid of all arrays
            // strings, then char**, then length, then addresses
            for (int i = 0; i < numArgs; i++) {
                delete[] argArr[i];
            }

            delete[] argArr;
            delete[] argLengthArr;
            delete[] argAddrs;

            switchToUser(e, newEsp, 0x12345678);
            return 0;
			break;
        }
		case 14: {
			// int rc = pipe(int* out, int* in)
            if (!validEsp((uint32_t)userStack, 1)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];
            uint32_t arg1 = userStack[2];

			// what is the logic here?
			// get two fd's if possible
			//		if not return -1
			// I think this take more work than that, though...


			// check to make sure out and in are valid
			if (!validPipeDescriptor(arg, arg1)) {
				return -1;
			}

            // make two bounded buffers
            StrongPtr<BoundedBuffer<char>> buf0 = StrongPtr<BoundedBuffer<char>>(new BoundedBuffer<char>(100));
            StrongPtr<BoundedBuffer<char>> buf1 = StrongPtr<BoundedBuffer<char>>(new BoundedBuffer<char>(100));

            // make two pipes with swapped read/write buffers
            StrongPtr<File> pipe0 = StrongPtr<File>(new Pipe(buf0, buf1));
            StrongPtr<File> pipe1 = StrongPtr<File>(new Pipe(buf1, buf0));

            // now attempt to open two file descriptors to them
            int fd0 = Thread::current()->process->addFile(pipe0);
            int fd1 = Thread::current()->process->addFile(pipe1);

            // check if both are valid
            if (fd0 == -1 || fd1 == -1) {
                Thread::current()->process->closeFile(fd0);
                Thread::current()->process->closeFile(fd1);
                return -1;
            }

			((int*)arg)[0] = fd0;
			((int*)arg1)[0] = fd1;

			return 0;
		}
		case 15: {
			// int thread(int eip, int esp)
			// make a new thread. It shares the file descriptor table and address space.
			// similar to fork. look at it for reference!!
            if (!validEsp((uint32_t)userStack, 1)) {
                return -1; 
            }   
            uint32_t arg = userStack[1];
            uint32_t arg1 = userStack[2];

			// make a switch param for our new thread
			SwitchParam param = SwitchParam(arg, arg1, 0); 

            // create new thread, has parent's process
			StrongPtr<Process> currProcess = Thread::current()->process;
            Thread* cThread = new FuncThread<SwitchParam>(currProcess, forkSwitch, param);

			// done with pointer to parent process
			currProcess.reset();

            // now add child's thread's exitEvent; will serve as the id of the child
            int cId = cThread->id;

            int pId = Thread::current()->id;

            // create new ThreadRemains object for the newly created thread
            StrongPtr<ThreadRemains> rem = StrongPtr<ThreadRemains>(
                    new ThreadRemains(cThread->exitEvent, pId));
            // set this child threads elemtnt accordingly in master list
            Thread::remains()[cId] = rem;


			//Debug::printf("made thread with id: %d\n", cId);
	
            // make this thread run!!!
            Thread::start(cThread);

			return cId;
		}
        default:
            Debug::panic("unknown system call %d",eax);
            return 0;
			break;
    }
    
}

extern "C" void sysAsmHandler();

void SYS::init(void) {
    IDT::trap(48,(uint32_t)sysAsmHandler,3);
	lock = new BlockingLock();
}

