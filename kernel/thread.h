#ifndef _THREAD_H_
#define _THREAD_H_

#include "stdint.h"
#include "refs.h"
#include "process.h"

class SpinLock;
class Event;
class Process;

struct ThreadRemains {
	StrongPtr<Event> childExitEvent;
	int exitStatus;
	int parentId;

	ThreadRemains(StrongPtr<Event> event, int parentId);
	virtual ~ThreadRemains();
};

struct Thread {
    static constexpr int STACK_LONGS = 1024;
    StrongPtr<Thread> next;
    uint32_t esp;
    uint32_t stack[STACK_LONGS];

    static volatile int nextId;

    void push(uint32_t v) {
        esp -= 4;
        *((uint32_t*) esp) = v;
    }

    Thread(StrongPtr<Process> process);
    virtual ~Thread();

    virtual void run() = 0;

    StrongPtr<Process> process;
    StrongPtr<Event> exitEvent;
	StrongPtr<Event> deleteEvent;
    int id;

    static void exit(void);
    static void yield(void);
    static StrongPtr<Thread> start(Thread *);
    static void init(void);
    static void shutdown(void);
    static StrongPtr<Thread> current(void);

	static StrongPtr<ThreadRemains>* remains();

    static int nCreate;
    static int nStart;
    static int nDelete;
};

template <class T>
class FuncThread : public Thread {
    void (*func)(T);
    T arg;
public:
    FuncThread(StrongPtr<Process> process, void (*func)(T), T arg) : Thread(process), func(func), arg(arg) {}
    void run() {
        func(arg);
    }
};

template <class T>
StrongPtr<Thread> threadCreate(StrongPtr<Process> process, void (*func)(T), T arg) {
    return Thread::start(new FuncThread<T>(process,func,arg));
}

class SimpleThread : public Thread {
    void (*func)(void);
public:
    SimpleThread(StrongPtr<Process> process, void (*func)(void));
    virtual ~SimpleThread();
    void run();
};

inline StrongPtr<Thread> threadCreate(StrongPtr<Process> process, void (*func)(void)) {
    return Thread::start(new SimpleThread(process,func));
}

extern void threadBlock(SpinLock *lock, bool was);
extern void threadReady(StrongPtr<Thread> t);
extern int threadId(void);

#endif
