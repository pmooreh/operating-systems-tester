#include "thread.h"
#include "stdint.h"
#include "debug.h"
#include "pic.h"
#include "heap.h"
#include "machine.h"
#include "queue.h"
#include "locks.h"
#include "refs.h"
#include "init.h"

ThreadRemains::ThreadRemains(StrongPtr<Event> event, int parentId) :
	childExitEvent(event),
	exitStatus(-1),
	parentId(parentId) {}

ThreadRemains::~ThreadRemains() {
	childExitEvent.reset();
}

struct ThreadInfo {
    StrongQueue<Thread> readyQ;
    StrongQueue<Thread> reaperQ;
    SpinLock contextLock;
    StrongPtr<Thread> activeThread;
	int* exitStatusArr;
	StrongPtr<ThreadRemains>* threadRemains;
	bool initDead;
};

static ThreadInfo *info = nullptr;

volatile int Thread::nextId = 0;

int Thread::nCreate = 0;
int Thread::nStart = 1;   // We don't have to start the initial thread
int Thread::nDelete = 0;

class FirstThread : public Thread {
public:
    FirstThread(StrongPtr<Process> process) : Thread(process) {}
    virtual void run() {
    }
};

/***************************************/
/* initialize the threading sub-system */
/***************************************/

void Thread::init(void) {
    info = new ThreadInfo();
    info->activeThread = StrongPtr<Thread> { new FirstThread(Process::kernel()) };
    info->activeThread->process->addressSpace->activate();
	info->exitStatusArr = new int[2020];
	info->threadRemains = new StrongPtr<ThreadRemains>[2020];
    info->initDead = false;
	tssEsp0 = (uint32_t) &info->activeThread->stack[STACK_LONGS];
}

/***************/
/* Constructor */
/***************/

Thread::Thread(StrongPtr<Process> process) : esp((uint32_t)(&stack[STACK_LONGS])),
    process(process),
    exitEvent(new Event()),
	deleteEvent(new Event()),
    id(getThenIncrement(&nextId,1))
{
    //Debug::printf("Thread %d:%d\n",process->id,id);
    getThenIncrement(&nCreate,1);
}

/**************/
/* Destructor */
/**************/

Thread::~Thread() {
    //Debug::printf("leaving %d:%d\n",process->id,id);
    getThenIncrement(&nDelete,1);
    deleteEvent->signal();
}

StrongPtr<Thread> Thread::current(void) {
    return info->activeThread;
}

extern "C" void contextSwitch(uint32_t newEsp, uint32_t *oldEsp);

int threadId(void) {
    return (info->activeThread.isNull()) ? -1 : info->activeThread->id;
}

static void reaper() {
    while (true) {
        bool was = info->contextLock.lock();
        if (!was) {
            /* Don't want to touch the heap with interrupts disabled */
            info->contextLock.unlock(was);
            return;
        }
        StrongPtr<Thread> t = info->reaperQ.remove();
        info->contextLock.unlock(was);
        if (t.isNull()) return;
        t.reset();
    }
}

/* Switch from the current thread after it has been
   placed on a queue */
/* holding contextLock */
void threadSwitch(int was) {
    StrongPtr<Thread> old = info->activeThread;

    StrongPtr<Thread> next = info->readyQ.remove();

    if (next.isNull() && !info->initDead) {
        while (!info->initDead);;
    }   

    if (next.isNull() && info->initDead) {
        old.reset();
        //info->activeThread.reset();
        info->contextLock.unlock(true);
        reaper();
        kernelTerminate();
        Debug::panic("no threads\n");
    }   


    if (old != next) {
        info->activeThread = next;
        uint32_t newEsp = next->esp;
        next.reset();
        uint32_t* pOldEsp = &old->esp;
        old.reset();
        info->activeThread->process->addressSpace->activate();
        tssEsp0 = (uint32_t) &info->activeThread->stack[Thread::STACK_LONGS];
        contextSwitch(newEsp,pOldEsp);
    }   

    info->contextLock.unlock(was);
}

/*
 * Add the active thread to the given queue then release
 * the given lock.
 *
 * Pre-conditions:
 *    - lock is held (in order to prevent the active thread
 *      from being added to the ready queue
 *    - interrupts are disabled
 */
void threadBlock(SpinLock *lock, bool was) {
    info->contextLock.lock();         /* Get the context lock */
    lock->unlock(false);         /* Unlock the queue while holding the context
                                    lock */
    threadSwitch(was);
}

void Thread::yield(void) {
    if (info == nullptr) return;
    if (info->activeThread.isNull()) return;
    reaper();
    bool was = info->contextLock.lock();
    info->readyQ.add(info->activeThread);
    threadSwitch(was);
}

void Thread::exit(void) {
    //Debug::printf("exit %d:%d\n",current()->process->id,current()->id);
    if (current()->id == 0)
		info->initDead = true;
	reaper();
    info->activeThread->exitEvent->signal();
    bool was = info->contextLock.lock();
    info->reaperQ.add(info->activeThread);
    threadSwitch(was);
}

StrongPtr<ThreadRemains>* Thread::remains() {
	return info->threadRemains;
}

void threadReady(StrongPtr<Thread> t) {
    bool was = info->contextLock.lock();
    info->readyQ.add(t);
    info->contextLock.unlock(was);
}

static void threadEntry() {
    info->contextLock.unlock(true);
    reaper();
    info->activeThread->run();
    Thread::exit();
    Debug::panic("should never get here\n");
}

StrongPtr<Thread> Thread::start(Thread *ptr) {
    reaper();

    getThenIncrement(&nStart,1);

    ptr->push((uint32_t) threadEntry);
    for (int i=0; i<7; i++) ptr->push(0); // regs

    auto res = StrongPtr<Thread> { ptr };
    //ptr->me = res;
    
    threadReady(res);
    return res;
}

/****************/
/* SimpleThread */
/****************/

SimpleThread::SimpleThread(StrongPtr<Process> process, void (*func)(void)) : Thread(process), func(func) {}

SimpleThread::~SimpleThread() {
}

void SimpleThread::run() {
    func();
}
