#ifndef _REFS_H_
#define _REFS_H_

#include "machine.h"
#include "debug.h"

inline uint32_t xchg(volatile uint32_t *p, uint32_t setVal) {
  uint32_t res;
  __asm__ volatile( "lock xchg %0,%1"
    : "=r" (res), "+m" (*p)
    : "0" (setVal)
    : "memory");
  return res;
}

template <class T> class StrongPtr;

template <class T>
class PtrState {
    volatile int count;
    T* ptr;

    PtrState(T* ptr) : count(1), ptr(ptr) {
    }

    friend class StrongPtr<T>;
    
};

template <class T>
class StrongPtr {
    PtrState<T> *state;

    void ref(PtrState<T>* s) {
        if (state != nullptr) {
            Debug::panic("StrongPtr::ref state is not null");
        }
        if (s != nullptr) {
            getThenIncrement(&s->count,1);
        }
        state = s;
    }

    void unref() {
        auto old = (PtrState<T>*) xchg((uint32_t*)&state,(uint32_t)nullptr);
        if (old != nullptr) {
            int x = getThenIncrement(&old->count,-1);
            if (x == 1) {
                delete old->ptr;
                old->ptr = nullptr;
                delete old;
            }
        }
    }

public:
    StrongPtr() : state(nullptr) {
    }

    explicit StrongPtr(T* ptr) : state((ptr == nullptr) ? nullptr : new PtrState<T>(ptr)) {
    }

    StrongPtr(const StrongPtr& src) : state(0) {
        ref(src.state);
    }

    StrongPtr(StrongPtr&& src) : state(src.state) {
        src.state = nullptr;
    }

    ~StrongPtr() {
        unref();
    }

    T* operator -> () const { return state->ptr; }

    bool isNull() const { return state == nullptr; }

    void reset() {
        unref();
    }

    StrongPtr<T>& operator = (const StrongPtr& src) {
        if ((this != &src) && (this->state != src.state)) {
            unref();
            ref(src.state);
        }
        return *this;
    }

    StrongPtr<T>& operator = (StrongPtr&& src) {
        if (this != &src) {
            unref();
            state = src.state;
            src.state = nullptr;
        }
        return *this;
    }

    bool operator ==(const StrongPtr<T>& other) const {
        return state == other.state;
    }

    bool operator !=(const StrongPtr<T>& other) const {
        return state != other.state;
    }
};

#endif
