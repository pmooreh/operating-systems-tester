#include "elf.h"
#include "machine.h"
#include "refs.h"
#include "fs.h"

uint32_t ELF::load(StrongPtr<File> file) {
    Elf32_Ehdr hdr;

    file->readAll(0,&hdr,sizeof(Elf32_Ehdr));

    uint32_t hoff = hdr.e_phoff;

    for (uint32_t i=0; i<hdr.e_phnum; i++) {
        Elf32_Phdr phdr;
        file->readAll(hoff,&phdr,sizeof(Elf32_Phdr));
        hoff += hdr.e_phentsize;

        if (phdr.p_type == PT_LOAD) {
            char *p = (char*) phdr.p_vaddr;
//            uint32_t memsz = phdr.p_memsz;
            uint32_t filesz = phdr.p_filesz;

//            Debug::printf("vaddr:%x memsz:0x%x filesz:0x%x fileoff:%x\n",
//                p,memsz,filesz,phdr.p_offset);
            file->readAll(phdr.p_offset,p,filesz);
            // assumes memory is already zero-filled
        }
    }

//    Debug::printf("entry:%x\n",hdr.e_entry);

    return hdr.e_entry;
}
