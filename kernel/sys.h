#ifndef _SYSCALL_H_
#define _SYSCALL_H_

#include "locks.h"

class SYS {
public:
    static void init(void);
	static BlockingLock* lock;
};

#endif
