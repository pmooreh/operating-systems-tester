#include "ide_support.h"
#include "machine.h"
#include "stdint.h"
#include "idt.h"
#include "pic.h"
#include "debug.h"
#include "thread.h"

// The drive number encodes the controller in bit 1 and the channel in bit 0

static inline int controller(uint32_t drive) {
    return (drive >> 1) & 1;
}

static inline int channel(uint32_t drive) {
    return drive & 1;
}

// the base I/O port for each controller

static int ports[2] = { 0x1f0, 0x170 };

static inline int port(uint32_t drive) {
    return ports[controller(drive)];
}

//////////////////
// drive status //
//////////////////

static inline long getStatus(uint32_t drive) {
    return inb(port(drive) + 7);
}

// Status bits
#define ERR     0x01
#define DRQ     0x08
#define SRV     0x10
#define DF      0x20
#define DRDY	0x40
#define BSY	0x80
    
/* Simple polling PIO interface
   Need to use interrupts and DMA
 */

static void waitForDrive(uint32_t drive) {
    uint8_t status = getStatus(drive);
    if ((status & (ERR | DF)) != 0) {
        Debug::panic("drive error, device:%x, status:%x",drive,status);
    }
    if ((status & DRDY) == 0) {
        Debug::panic("drive %x is not ready, status:%x",drive,status);
    }
    while ((getStatus(drive) & BSY) != 0) {
        Thread::yield();
    }
}

static uint32_t nRead = 0;

void readSector(uint32_t drive, uint32_t sector, uint32_t* buffer) {

    //Debug::say("readSector %d",sector);

    nRead += 1;
    int base = port(drive);
    int ch = channel(drive);

    waitForDrive(drive);

    outb(base + 2, 1);			// sector count
    outb(base + 3, sector >> 0);	// bits 7 .. 0
    outb(base + 4, sector >> 8);	// bits 15 .. 8
    outb(base + 5, sector >> 16);	// bits 23 .. 16
    outb(base + 6, 0xE0 | (ch << 4) | ((sector >> 24) & 0xf));
    outb(base + 7, 0x20);		// read with retry

    waitForDrive(drive);

    while ((getStatus(drive) & DRQ) == 0) {
        Thread::yield();
    }

    for (uint32_t i=0; i<IDE_SECTOR_SIZE/sizeof(uint32_t); i++) {
        buffer[i] = inl(base);
    }
}

void ideStats(void) {
    Debug::say("nRead %d",nRead);
}
