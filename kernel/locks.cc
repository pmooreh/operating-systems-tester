#include "locks.h"

SpinLock::SpinLock() : flag(0) {}

bool SpinLock::tryLock() {
    uint32_t oldValue = cmpxchg(&flag,0,1);
    return (oldValue == 0);
}

bool SpinLock::lock() {
    while (true) {
        bool was = picDisable();
        if (tryLock()) return was;
        picRestore(was);
    }
}

void SpinLock::unlock(bool was) {
    flag = 0;
    picRestore(was);
}

Semaphore::Semaphore(uint32_t count) : count(count) {}

void Semaphore::down() {
    bool was = lock.lock();
    if (count > 0) {
        count --;
        lock.unlock(was);
    } else {
        waiting.add(Thread::current());
        threadBlock(&lock,was);
    }
}

void Semaphore::up() {
	bool was = lock.lock();
    StrongPtr<Thread> t = waiting.remove();
    if (t.isNull()) {
        count ++;
        lock.unlock(was);
    } else {
        lock.unlock(was);
        threadReady(t);
    }
}

BlockingLock::BlockingLock() : sem(1) {}
void BlockingLock::lock() { sem.down(); }
void BlockingLock::unlock() { sem.up(); }

Event::Event() : sem(0) {
}
Event::~Event() {
}
void Event::wait() {
    sem.down(); sem.up();
}
void Event::signal() {
    sem.up();
}

Barrier::Barrier(uint32_t n) : sem(0), togo(n), lock() {}

void Barrier::sync() {
    bool doit;

    lock.lock();
    doit = (togo == 1);
    if (togo != 0) {
        togo --;
    }
    lock.unlock();

    if (!doit) {
        sem.down();
    }
    sem.up();
}
