#include "stdint.h"
#include "debug.h"
#include "refs.h"

#include "vmm.h"
#include "ide.h"
#include "fat439.h"
#include "elf.h"

void kernelMain(void) {

    Debug::printf("kernelMain pid:%d\n",Thread::current()->process->id);

    StrongPtr<BlockIO> d = IDE::d();
    StrongPtr<Fat439> fs { new Fat439(d) };
    StrongPtr<Directory> root = fs->rootdir;
    StrongPtr<File> initFile = root->lookupFile("init");

    uint32_t entry = ELF::load(initFile);

    switchToUser(entry,0xffffff00, 0x12345678);
}

/* Called when the system is about to shutdown */
void kernelTerminate() {
}
