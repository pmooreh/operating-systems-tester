#include "libc.h"

int main(int argc, char** argv) {
    for (int i=1; i<argc; i++) {
        int fd = open(argv[i]);
        if (fd < 0) {
            putstr("*** "); putstr(argv[0]); putstr(": ");
            putstr(argv[i]); putstr(": No such file or directory\n");
            return -1;
        } else {
            char buf[100];
            while(1) {
                long n = read(fd,buf,100);
                if (n == 0) break;
                if (n < 0) {
                    putstr("*** "); putstr("error reading : "); putstr(argv[i]); putstr("\n");
                    break;
                }
                for (int j = 0; j < n; j++) {
                    putch(buf[j]);
                }
            }
        }
        close(fd);
    }
    return 0;
}
