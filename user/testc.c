#include "libc.h"

int main() {
    putstr("*** hello\n");

    int sem = semaphore(0);

    putstr("*** open hello.txt\n");
    int fd = open("hello.txt");
    SHOW(fd);

    int id = fork();

    if (id == 0) {
        /* child */
        
        saydec("child len",len(fd));
        up(sem);
    } else {
        /* parent */
        down(sem);
        saydec("parent len",len(fd));
    }

    return 0;
}
