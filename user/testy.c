#include "libc.h"

int main(){
    putstr("*** hello\n");
    putstr("*** semaphore(0)\n");
    int sem = semaphore(0);

    putstr("*** down(), about to deadlock\n");
    down(sem);

    putstr("*** should never get here\n");
}
