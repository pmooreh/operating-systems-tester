#ifndef _SYS_H_
#define _SYS_H_

/****************/
/* System calls */
/****************/

typedef int ssize_t;
typedef unsigned int size_t;

/* exit */
extern void exit(int rc);

/* create a sempahore, return something that represents it */
extern int semaphore();

/* say up to a semaphore */
extern int up(int s);

/* say down to a semaphore */
extern int down(int s);

/* fork */
extern int fork();

/* join */
extern int join(int id);

/* shutdown */
extern void shutdown();

/* open */
extern int open(char* name);

/* close */
extern void close(int id);

/* read */
extern ssize_t read(int fd, void* buf, size_t nbyte);

/* write */
extern ssize_t write(int fd, void* buf, size_t nbyte);

/* len */
extern ssize_t len(int fd);

/* seek */
extern ssize_t seek(int fd, size_t offset);

/* exec */
extern int exec(char* prog, int n, ...);

/* pipe */
extern int pipe(int* fd0, int* fd1);

/* thread */
extern int thread(int eip, int esp);

#endif
