#include "libc.h"
/**************************************
 * files
 */

int fileTest() {
	// open file with bad char*
	char* bad = (char*)0x10000000;
	putstr("bad string\n");
	int fd = open(bad);
	if (fd != -1) {
		putstr("bad string");
		return -1;
	}

	putstr("open file with overrunnning title\n");
	bad = (char*)0xfffffffd;
	bad[0] = 'h';
	bad[1] = 'e';
	bad[2] = 'l';
	fd = open(bad);
	if (fd != -1) {
		return -1;
	}

	// open invalid file
	putstr("invalid file\n");
	fd = open("he.txt");
	if (fd != -1) {
		putstr("invalid file");
		return -1;
	}

	// ops on invalid fd
	int status = 0;
	char buf[10];
	putstr("seek\n");
	status = seek(10, 100);
	if (status != -1) {
		putstr("seek");
		return -1;
	}
	putstr("write\n");
	status = write(10, buf, 10);
    if (status != -1) {
        putstr("write");
        return -1; 
    }
	putstr("read\n");
    status = read(10, buf, 10);
    if (status != -1) {
        putstr("read");
        return -1;
    }

	int actualFd = open("hello.txt");

	// writing to fat439 file returns -1
	putstr("write to fat439 file\n");
	status = write(actualFd, buf, 4);
	if (status != -1) {
		return -1;
	}

	// ops with invalid buffer pointer
	char* badBuf = (char*)0x01000000;
	putstr("bad read buf\n");
	status = read(actualFd, badBuf, 10);
	if (status != -1) {
		putstr("bad read buf");
		return -1;
	}
	putstr("bad write buf\n");
	status = write(actualFd, badBuf, 10);
	if (status != -1) {
		putstr("bad write buf");
		return -1;
	}

	// reset
	seek(actualFd, 0);	
	putstr("read with overlapping buffer\n");
	// this one has valid bytes: c, d, e, f
	// so should return 4
	badBuf = (char*)0xfffffffc;
	status = read(actualFd, badBuf, 10);
	if (status != 4) {
		return -1;
	}
	badBuf = (char*)((int)badBuf + status);
	status = read(actualFd, badBuf, 10);
	if (status != -1) {
		return -1;
	}

	// reset
	seek(actualFd, 0);
	putstr("read with negative length\n");
	status = read(actualFd, buf, -3);
	putstr(buf);
	if (status != -1) {
		return -1;
	}

	putstr("seek with negative offset\n");
	status = seek(actualFd, -5);
	if (status != -1) {
		return -1;
	}

	return 0;
}
/**************************************
 * pipes
 */
int pipeTest() {
	// edge of memory
	int* in = (int*)0xfffffffd;
	int* out = (int*)0xfffffff0;

	int status = 0;

	// edge of memory descriptor
	status = pipe(in, out);
	if (status != -1) {
		putstr("edge of memory\n");
		return -1;
	}

	// overlapping pipe descriptors
	in = (int*)0xfffffff0;
	for (int i = 0; i < 4; i++) {
		int* p = (int*)((int)in + i);
		status = pipe(p, out);
		if (status != -1) {
			putstr("overlapping descriptors\n");
			return -1;
		}
	}

	// pipe descriptors in kernel space
	in = (int*)0x00001000;
	out = (int*)0x00000010;
	status = pipe(in, out);
	if (status != -1) {
		putstr("kernel space descriptor");
		return -1;
	}

	putstr("actually testing pipes now\n");
	int a = 0;
	int b = 0;
	char aBuf[6];
	char bBuf[6];
	bBuf[0] = 'h';
	bBuf[1] = 'o';
	bBuf[2] = 'w';
	bBuf[3] = 'd';
	bBuf[4] = 'y';
	bBuf[5] = 0;
	
	pipe(&a, &b);
	status = write(b, bBuf, 6);
	if (status != 6) {
		return -1;
	}
	status = read(a, aBuf, 6);
	if (status != 6) {
		return -1;
	}
	
	putstr("negative value for write\n");
	status = write(b, bBuf, -5);
	if (status != -1) {
		return -1;
	}

	putstr("negative val for read\n");
	status = read(a, aBuf, -3);
	if (status != -1) {
		return -1;
	}

	putstr("bad buffers for write and read\n");
	char* badBuf = (char*)0x10000000;
	status = write(b, badBuf, 3);
	if (status != -1) {
		return -1;
	}
	status = read(a, badBuf, 3);
	if (status != -1) {
		return -1;
	}

	putstr("buffers on edge of memory\n");
	badBuf = (char*)0xfffffffc;
	status = write(a, badBuf, 10);
	if (status != 4) {
		return -1;
	}
	badBuf = (char*)0xfffffffd;
	status = read(b, badBuf, 10);
	if (status != 3) {
		return -1;
	}

	return 0;
}
/**************************************
 * joins
 */
int joinTest() {
	putstr("join with negative thread\n");
	int rc = join(-5);
	if (rc != -1) {
		return -1;
	}

	putstr("join wih thread 0 (yourself)\n");
	rc = join(0);
	if (rc != -1) {
		return -1;
	}

	putstr("fork, then join twice\n");
	int id = fork();
	if (id == 0) {
		rc = join(0);
		if (rc != -1) {
			exit(-1);
		}
		exit(42);
		//putstr("child made it\n");
	}
	else {
		rc = join(id);
		putstr("joined child\n");
		if (rc != 42) {
			return -1;
		}
		rc = join(id);
		if (rc != -1) {
			//putstr("joined twice and failed\n");
			return -1;
		}
	}

	putstr("fork twice, try to join with grandchild\n");
	int fdGP;
	int fdGC;
	pipe(&fdGP, &fdGC);

	int s = semaphore(0);
	int id0 = fork();
	int id1 = fork();

	int gc;
	if (id0 != 0 && id1 != 0) {
		read(fdGP, ((char*)(&gc)), 4);
		int status = join(gc);
		if (status != -1) {
			saystr("oops");
			exit(-1);
		}
	}
	if (id0 == 0 && id1 != 0) {
		write(fdGC, ((char*)(&id1)), 4);
	}
	// clear them all but one!
	if (id0 != 0 || id1 != 0) {
		up(s);
		exit(0);
	}

	down(s);
	down(s);
	down(s);
	return 0;
}
/**************************************
 * semaphores
 */
int semaTest() {
	putstr("pass negative number in as sema count\n");
	int sema = semaphore(-1);
	if (sema != -1) {
		return -1;
	}

	putstr("ops on invalid semaphore ids\n");
	up(3);
	down(38);
	up(-20);
	down(-48);

	putstr("semaphore stress\n");
	while (sema < 2015) {
		sema = semaphore(1);
		up(sema);
		down(sema);
	}

	return 0;
}
/**************************************
 * threads and forks
 */
int func(void* ptr) {
	((int*)ptr)[0] = 76;
	return 17;
}

int threadTest() {
	putstr("check bad esp\n");
	int child = thread(((int)(&func)), 0x100);
	int status = join(child);
	if (status != -1) {
		return -1;
	}

	putstr("check bad eip\n");
	int ptr = (int)(malloc(30));
	child = thread(0x89, ptr);
	status = join(child);
	if (status != -1) {
		return -1;
	}

	putstr("check to make sure address space is shared\n");
	int x = 0;
	child = threadCreate(func, (void*)(&x));
	status = join(child);
	if (status != 17) {
		SHOW(status);
		return -1;
	}
	if (x != 76) {
		SHOW(x);
		return -1;
	}

	putstr("check addr space isnt share when forking\n");
	int id = fork();
	if (id == 0) {
		x = 39;
		exit(23);
	}
	else
		status = join(id);
	if (status != 23) {
		SHOW(status);
		return -1;
	}
	if (x != 76) {
		SHOW(x);
		return -1;
	}

	return 0;
}
/* * * * * * * * * * * * * * * * * * * 
 * main
 */
int main() {
    putstr("*** hello\n");
	putstr("\n");

	int passedAll = 0;

	putstr("pipe woes\n");
	passedAll += pipeTest();
	putstr("\n");

	putstr("join woes\n");
	passedAll += joinTest();
	putstr("\n");

	putstr("file woes\n");
	passedAll += fileTest();
	putstr("\n");

	putstr("thread woes\n");
	passedAll += threadTest();
	putstr("\n");

    putstr("semaphore woes\n");
    passedAll += semaTest();
    putstr("\n");

	if (passedAll == 0)
	saystr("yay");

    return 0;
}

