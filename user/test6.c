#include "libc.h"

#define N 2000
// Enough to catch not freeing the PD

int main() {
    putstr("*** hello\n");
    for (int i = 0; i < N; i++) {
        int id = fork();
        if (id == 0) {
            exit(i+1000000);
        }
        else {
            int rc = join(id);
            if (rc != i+1000000) {
                putstr("\n*** rc mismatch, expected ");
                putdec(i+1000000),
                putstr(" found ");
                putdec(rc);
                putstr("\n");
                return -1;
            }
            if ((i % 100) == 0) {
                putstr("*** ");
                putdec(i);
                putstr("\n");
            }
        }
    }
    putstr("*** goodbye\n");
    return 0;
}
