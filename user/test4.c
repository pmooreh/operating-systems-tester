#include "libc.h"

int main() {
    putstr("*** hello\n");

    putstr("*** fork\n");
    int id = fork();

    if (id == 0) {
        /* child */
        putstr("*** child\n");
        return 42;
    } else {
        /* parent */
        int rc = join(id);
        putstr("*** parent\n");
        putstr("*** rc = "); putdec(rc); putstr("\n");
    }

    return 0;
}
