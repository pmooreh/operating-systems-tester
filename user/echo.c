#include "libc.h"

int main(int argc, char** argv) {
    putstr("*** ");
    char *sep = "";
    for (int i=1; i<argc; i++) {
        putstr(sep);
        sep = " ";
        putstr(argv[i]);
    }
    putstr("\n");
    return 0;
}
   
