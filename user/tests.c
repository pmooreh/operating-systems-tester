#include "libc.h"
#include "sys.h"

#define NULL 0
char *cmd = NULL;

void go() {
    if (cmd == NULL) {
        putstr("*** Command not given\n");
    }
    putstr("*** $ ");
    putstr(cmd);
    putstr("\n");
    int id = fork();
    if (id == 0) {
        int ret = exec(cmd,0);
        if (ret == -1) {
            putstr("*** Command not found\n");
            exit(0);
        }
        else {
            putstr("*** Weird things happened\n");
            exit(0);
        }
    }
    else {
        join(id);
        return;
    }
}

int main() {
    cmd = "echo";
    go();
    cmd = "aoeuidht";
    go();
    return 0;
}
